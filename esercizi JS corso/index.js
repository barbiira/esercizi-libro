var x = 10;
var y = 'ciaone';  // Dinamically Typed
console.log(x + y);

//-----------------
//Firma della funzione
//Nelle tonde vanno gli argomenti

function nomefunzione () {
    //Corpo della funzione (body)
};

//FUNCTION DECALARATION = funzioni dichiarate con nome
//A e B sono PARAMETRI FORMALI(perchè formalmente li abbiamo chiamati a e b, sulla linea della firma)
    //Return 
    //Restituisce il calore descritto a destra
    //Termina l'esecuzione della funzione (quello che viene dopo che sia "console" o un altro "return")
function sum (a, b){
    return a+b;
}

//E' meglio dichiarare PRIMA e DOPO invocare.
//Per invocare una funzione si usano il nome e le ()
//I valori indicati sotto vengono quindi chiamati PARAMETRI ATTUALI ex:(3, 2)
sum(3, 2);

//Ci sono piu modi per dichiarare una funzione o non dichiararla.
//La funzione 'anonima' non ha nome : function (){};
//FUNCTION EXPRESSION: è anonima perchè non ha nome, contiene la funzione quindi, ma NON è la funzione.

var x = function (){
}
//quindi per invocarla si utilizza sempre lo stesso sistema: contenitore()
sum ()
x();
x = 10;

//COSTRUTTO DI SELEZIONE: permette di specificare le istruzioni da eseguire al verifiarsi di una determinata situazione (if , else)
function checkAge(age) {
    //if (condizione da eseguire se la condizione è verificata)
    //Il < ha un codice in precedenza (RICORDA TABELLA)
    //FALSE e UNDEFINED in booleano restituiscono entrambe FALSE.
    
    if (age < 18) {
        return false;
// in caso di doppio return possiamo quindi omettere else {}
     return true;
     
}










