//FUNZIONE INVERTI NOME:

function invertiNome(str){
  str.substr(0, 1);
    if (str.length >= 1) {
        return invertiNome(str.substr(1)) + nomeInvertito;
    }
    
    return str;
}

console.log(invertiNome("Roma"));

//////////////////////////////////////////////////////////

function invertiNome(str) {
  if (str !== ""){
    return invertiNome(str.substr(1)) + str.charAt(0);}
    //Il metodo charAt in JavaScript utilizzato sulle stringhe restituisce un carattere di una stringa. La posizione del carattere è indicata nell’indice tra parantesi tonde.

   return ""; //quando terminerà l'estrazione delle lettere.
}

console.log(invertiNome("Roma"));

////////////////////////////////////////

//Canzoni che durano più di 2.30 minuti

var canzoni = [
    { canzone: '7+3', artista:'Ultimo', durata: 1.40 },
    { canzone: 'Copines', artista:'Aya', durata: 1.38 },
    { canzone: 'Scetate Vuajo', artista:'Mannarino', durata: 1.30 },
    { canzone: 'Ad Occhi Chiusi', artista:'M.Mengoni', durata: 1.50 },
    { canzone: 'Ahia!', artista:'Pinguini', durata: 1.30}
];
//Definisco un array di canzoni con la durata
//poi creo una var con il metodo filter di "canzoni", mi creo poi
//una funzione con il ritorno della durata < 138 sec (ovvero 2.30 m)
//grazie al metodo filter che crea un nuovo array con tutti gli elementi che superano il test implementato dalla funzione callback. (funzione callback = richiama. )

var canzoneDurata = canzoni.filter ( function (a){
    
    return a.durata > 1.38;
});

//stampo poi il risultato della funzione
console.log(canzoneDurata);
