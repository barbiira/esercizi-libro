(var x = 10;
var y = 'ciaone';  // Dinamically Typed
console.log(x + y);

//-----------------
//Firma della funzione
//Nelle tonde vanno gli argomenti

function nomefunzione () {
    //Corpo della funzione (body)
};

//FUNCTION DECALARATION = funzioni dichiarate con nome
//A e B sono PARAMETRI FORMALI(perchè formalmente li abbiamo chiamati a e b, sulla linea della firma)
    //Return 
    //Restituisce il calore descritto a destra
    //Termina l'esecuzione della funzione (quello che viene dopo che sia "console" o un altro "return")
function sum (a, b){
    return a+b;
}

//E' meglio dichiarare PRIMA e DOPO invocare.
//Per invocare una funzione si usano il nome e le ()
//I valori indicati sotto vengono quindi chiamati PARAMETRI ATTUALI ex:(3, 2)
sum(3, 2);

//Ci sono piu modi per dichiarare una funzione o non dichiararla.
//La funzione 'anonima' non ha nome : function (){};
//FUNCTION EXPRESSION: è anonima perchè non ha nome, contiene la funzione quindi, ma NON è la funzione.

var x = function (){
}
//quindi per invocarla si utilizza sempre lo stesso sistema: contenitore()
sum ()
x();
x = 10;

//COSTRUTTO DI SELEZIONE: permette di specificare le istruzioni da eseguire al verifiarsi di una determinata situazione (if , else)
function checkAge(age) {
    //if (condizione da eseguire se la condizione è verificata)
    //Il < ha un codice in precedenza (RICORDA TABELLA)
    //FALSE e UNDEFINED in booleano restituiscono entrambe FALSE.
    
    if (typeof age === 'string');

    if (age < 18) {
        return false;
// in caso di doppio return possiamo quindi omettere else {}
    } return true;  
}
//  quindi possiamo semplificare tutto a:
//perchè il simbolo minore è già una condizione e non devo convertire in booleano.
function checkAge (age){
    return age <  18;
    } 
};
var ris = checkAge(17);// in caso è minore di 18 non fa accedere.


//Riesce a risolvere più casi, ad esmepio una parte num e letterale
parseInt('1a'); //ci ridà 1
parseInt ('1.35a'); //ci ridà 1 perchè rimuove le lettere
parseInt ('abc.135a.4') //ci ridà NAN perchè non è UN SOLO numero


//CLUSURE : una funzione che può accedere a tutti gli elementi dichiarati al suo esterno.
//AD ESEMPIO:
var v = 'ciaone';
function sum (a, b){
    console.log(v);
    var c = 10;
    var v = 'boh';
    return parseInt(a) + parseInt (b);
};
function fn(){
    console.log(v);
}

//OUTHER LEXICAL ENVAIRORMENT: Ad ogni parentesi graffa viene greato un OLE, quindi uno spazio dedicato di memoria diviso in due zone.
//Una zona con la dichiarazione degli elementi
//Una zona che LEXIAL ENVAIROMENT esterno, ovvero che mi conduce a ciò che sta fuori e mi serve da inserire all'interno.
//ESEMPIO: zona1: var c/ var ____ zona 2: var='ciaone'
//GLOBAL LEXIAL ENVARIORMENT: viene creato automaticamente per primo che contiene tutto ciò che è più esterno.

//GLI OPERATORI LOGICI: "and" o "or" o "not" .
//And= si applica con la && quando entrambi i valori sono verificati.
//Or= si applica con || , quando almeno uno dei due valori è verificato.
//Not= Si applica con ! __Negazione. Nega il valore es: 1 diventa 0 o True diventa Folse.
ESEMPIO:
 AND  &&
 0 0  0
 0 1  0
 1 0  0
 1 1  1
 
 OR  ||
 0 0  0
 0 1  1
 1 0  1
 1 1  1

NOT  !
0    1
1    0

//esempio stringa. Prima confronta cosa ho a sx e dx e poi se uno è < o >.
if (a > 20 && b < 12) {};
if (a > 20 || b < 12 ) {};


//GLI ARRAY
//E’ un elenco di elementi indicizzati (posizionati) che parte sempre dal numero 0.
//Vengono inseriti dentro una variabile:

var names = [
    ‘alessandro’ ,
     ‘omar’, 
     ‘francesca’
   ] ;

   //IL CICLO FOR:
   //Lo step viene eseguito al termine di ogni ciclo interativo
   // var i -> inizializzaione del ciclo for
// i < 3 -> Condizione: se è true, itera (esegue uno step iterativo)
//i = 1 + 1 -> è l’operazione.

   for (var i = 0; i < 3; i = i + 1 ) {
       var name = names[i];
   }


//________________________________________________________________

//Gli oggetti sono un agglomerato di chiavi ad ognuna delle quali è associato un valore
//In questo caso 'o' è l'oggetto.

var o = {
    //Le chiavi,negli oggetti mi permettono di definire delle proprietà
    key: 'value'
};

///////

var user = {
    email:'qualcosa@email.com',
    name:'Jerry',
    surname:'Calà'
}

var copia = Object.assign( { } , user);
console.log(user,copia);




////////////////////////////////////////////////////////////////////////////

//COME CREARE UN OGG A PARTIRE DA UNA FUNZIONE
//Si dà precedenza alla email, spesso, perchè è un elemento unico e personale per ogni persona.
//se io voglio associare ad una chiave (sx) un valore che è contenuto all'interno di una variale/parametro formale (valore che cambia)
//glielo passo così= email: email.
//Quando la chiave e il valore da associare ad essa è contenuto in una variabile/parametro contenuto con uno stesso nome 
//posso ometterne uno dei due

//Oggetto plane js: quando si restituisce un oggetto strutturato con le {}

/**
 * Return a new "user" object istance
 * @param {*} email 
 * @param {*} name 
 * @param {*} surname 
 * @returns 
 */

function createUser( email, name, surname) {
    return{
        email, //: email,
        name, //: name,
        surname, //:surname
    };
}


//Questo User (costruttore) deriva da un Object.
//Quindi non è più un Object generico ma si chiamerà User.
//Così si normalizza la struttura di un oggetto.

function User (email, name, surname){
    this.email=email;
    this.name=name;
    this.surname=surname;
    //Quando una proprietà contiene una funzione viene chiamata metodo
    //Questo metodo ci deve restiture "ciao nome e cognome"
    this.sayHi = function() {
        console.log ('Ciao' + this.name + '' + this.surname);

    }
} 
//La parola new invoca un NUOVO costruttore.'new' dice al costruttore eseguiti.
//Ogni volta che io invoco 'new', 'this' assume il valore che noi vediamo all'interno dello stack.  
//Ovvero l'inndirizzo della cella di memoria dove abbiamo indicato l'oggetto (nell'heap).
//Mentre dentro ad U1 avrò: un puntatore alla zona di memoria dello Stack che va all'Heap.
//Quelle nelle tonde sono i parametri che gli stiamo passando
var u1 = new User ('uno@uno.com', 'Uno', 'Primo');
var u2 = new User ('due@due.com', 'Due', 'Secondo');

//Per eseguirlo: Invochiamo la chiave + i parametri dentro le () se ci sono.
//Quindi quando invochiamo u1, ci stamperà 'CiaoUnoPrimo'
//u2 scriverà sempre 'CiaoDueSecondo'
u1.sayHi();
u2.sayHi();




//Se abbiamo un array e sulla var che contiene questo array aggiungiamo di seguito un punto accadono determinate cose.
//Un array è giustamente un contenitore che impila i nostri dati.
//il ".push" ci permette di inserire un elemento nella coda del nostro array (non nella testa, quindi non alza la nostra pila).
//Ad esempio nel carrello di amazon, quando aggiungiamo un elemento potrebbe essere "carrello.push"
//il "pop" restituisce l'ultimo elemento inserito da noi, o no, nella coda dell'elenco e lo leva dalla coda quando lo invochiamo.
//lo ".unshift" ci permette invece di levare gli elementi della pila e inserire quello che abbiamo deciso nella coda es: 30,65,40,12,23.
var numbers = [ 30, 40, 12, 23];
numbers.push(30, 65);


//è la PROPRIETA’ per sapere la lunghezza dell’array.
var numbers = [ 3, 4, 1, 2];
for (var i=0; i <numbers.length; i += 1) {
}









