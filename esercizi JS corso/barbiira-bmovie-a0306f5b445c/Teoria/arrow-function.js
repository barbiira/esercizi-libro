//ES5:
// function fnName( ){

// } //function Declaratin

// //function expression (anonima)
// var fn = function(){ 

// }


//________________________________________________________________________
//ES6
//Le Arrow function si chiamano così perchè ricordano una freccia --> "=>"
//Le arrow function sono funzioni anonime in ES6, ma differiscono
//nella sintassi, non c'è la keyword function
//Se non ho parametri metto le ()
//La  " =>" stacca la parte dichiarativa dal corpo
//const f = () => {};

//Quando ho un solo parametro formale posso omettere le parentesi ()
//Ad esempio: a => {}
//Non è quindi necessario scrivere (a) => {}
//
//Quando ho più di un parametro formale sono costretto a
//reitrodurre le parentesi ()
//Ad esempio : (a,b) => { corpo della funzione/istruzione }
//
//Quando il corpo della funzione è costituito da una sola istruzione
//le { } possono essere omesse
//Ad esempio: n => n * 2
//Il risultato ha un "return" implicito. Però questo solo quando
//c'è una sola istruzione, quindi quando le {} sono omesse.

// !! ATTENZIONE !! : In presenza delle { } il return va messo!!!


