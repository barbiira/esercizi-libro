// export function Movie(id, name, image) {
//   this.id = id;
//   this.name = name;
//   this.image = image;
// }

//In ES6 come dichiare un metodo/costruttore
export class Movie {
  //nomeMetodo()
  constructor(id, name, image) {
    this.id = id;
    this.name = name;
    this.image = image;
  }
  //Creiamo (dentro il costruttore Movie) un metodo che
  //restituisce il tag immagine della copertina
  getPosterTag() {
    //in ES5 -->  return '<img src="'+this.image+'">'
    //oppure -->  return '<img src="'.concat(this.image)'">'
    //In ES6
    //Backtick (template literal)
    //${} dice che c'è un valore da interpolare
    return ` <img class='poster' src="${this.image}"> `;
  }
}
