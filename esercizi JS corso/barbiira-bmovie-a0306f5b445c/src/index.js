// entry point
import "bootstrap/dist/js/bootstrap";
import { getMovies, sliceMovies } from "./functions/slider";
import { getUsers } from "./functions/users";

// getUsers(function (users) {
//   users.forEach(function (user) {
//     tbody.innerHTML =
//       "<tr><td>" +
//       user.id +
//       "</td>" +
//       user.email +
//       "</td><td>" +
//       user.username +
//       "</td></tr>";
//   });
// });
/////
function loadMovies(movie, resetIndexes) {
  if (resetIndexes) {
    from = 0;
    to = 3;
  }

  // Svuoto lo slider prima di mettere i film nuovi
  // (quelli richiesti attraverso la select)
  slider.innerHTML = "";

  getMovies(movie, function (apiMovies) {
    movies = apiMovies;

    // //Restituisco l'array
    // sliceMovies(movies, from, to).forEach(function (movie) {
    //   slider.innerHTML +=
    //   '<div class="col-lg-3"><img class="poster" src="' +
    //   movie.image +
    //   '"></div>';
    // });

    //Il ciclo "forOff" di ES6
    //Voglio iterare su tutti gli elementi dell'array
    const slices = sliceMovies(movies, from, to);
    //dichiaro una costante che ha le'emento su cui itero
    //of slices è l'array
    for (const slice of slices) {
      slider.innerHTML += `
      <div class="col-lg-3">
      ${slice.getPosterTag()}
      </div>
      `;
      // IN ES5 '<div class="col-lg-3">' + slice.getPosterTag() + "</div>";
    }
  });
}

let tbody = document.getElementById("tbody");

getUsers(function (users) {
  users.forEach(function (user) {
    tbody.innerHTML =
      "<tr><td>" +
      user.id +
      "</td>" +
      user.email +
      "</td><td>" +
      user.username +
      "</td></tr>";
  });
});

let slider = document.getElementById("slider");
let from = 0;
let to = 3;
let movies = [];

function handleSliderChange(isNext) {
  if (isNext) {
    // ++ è un operatore (restituisce un risultato)che esegue un incremento.
    //quindi se from : 0, con from++ mi dà il valore PRIMA che viene incrementato nello stack
    //Percui ++from mi dà lo STESSO valore che assume nello stack.
    from = from === movies.length - 1 ? 0 : ++from;
    to = to === movies.length - 1 ? 0 : ++to;
  } else {
    from = from === 0 ? movies.length - 1 : --from;
    to = to === 0 ? movies.length - 1 : --to;
  }

  slider.innerHTML = "";
  sliceMovies(movies, from, to).forEach(function (movie) {
    slider.innerHTML +=
      '<div class="col-lg-3"><img class="poster" src="' +
      movie.image +
      '"></div>';
  });
}

document.querySelectorAll("button.slider-action").forEach(function (b) {
  b.addEventListener("click", function (e) {
    handleSliderChange(e.target.id === "slider-next");
  });
});

//Per muovere i pulsanti con le freccette:
document.addEventListener("keydown", function (event) {
  //il preventDefault serve per non far andare in automatico il funzionamento delle freccette
  //che altrimento si andrebbero a selezionare la select cambiandone le voci.
  event.preventDefault();
  if (event.key === "ArrowRight") {
    handleSliderChange(true);
  } else if (event.key === "ArrowLeft") {
    handleSliderChange(false);
  }
});

// Individuo la select nel documento
let sel = document.querySelector('select[name="Film"]');

// Inizializzo lo slider con i film di Batman (almeno qualcosa ce trovo)
loadMovies("batman");

sel.addEventListener("change", function (e) {
  // Carico i film indicati dall'utente con la select
  loadMovies(e.target.value, true);
});

//
//
//Ciclo For-in per iterare sugli oggetti (off per gli array)
const o = {
  k1: "valore",
  k2: 10,
  chiappa2: {
    //
  },
};
//const key conterrà la chiave che di volta in volta viene
//analizzata dal for-in
for (const key in o) {
  // o.key => con questa forma accedo alla proprietà key nell'oggetto o
  //non è corretta perchèla costante key non esiste nell'oggetto' "o"

  // o[key]=> Accedo alla chiave presente in "key" (la prima volta conterrà k1 ecc)
  //all'interno dell'oggetto O, quindi vedrò solo il valore della chiave.
  //key,o[key] => mi fa vedere anche il nome della chiave
  console.log(key, o[key]);
}

// //in ES5 avremmo dovuto scrivere così:
// const keys = Object.key(o);
// keys.forEach(function (key) {
//   console.log(key, o[key]);
// });

// //in è anche un operatore di js che ci permette di capire se un ogg ha al suo interno una chiave
// const o2 = {
//   k1: undefined,
//   k2: 10,
// };
// // if (o2.k1) {
// //   console.log("k1 esiste");
// // }
// // if (o2.k2) {
// //   console.log("10 esiste");
// // } //Questo sistema non è valido per controllare l'esistenza di una chiave
// //Perchè ci va a verificare se le chiavi ci resistuiscono un valore booleano
// //Ovvero se accediamo a k1 e il suo valore è 'undefined' per lui è False
// //mentre quando accediamo a k2 troviamo il valore 10 che è true.
// //Se vogliamo trovare una chiave che non esiste possiamo invece aiutarci con l'operatore "in"
// if ("k1" in o2) {
//   console.log("k1 esiste");
// }

// if ("k2" in o2) {
//   console.log("k2 esise");
// }
